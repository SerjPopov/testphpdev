<?php

namespace common\services\user;

use common\models\User;

/**
 * Сервис для наполнения тестовыми данными таблицы User
 */
class TestUserService
{

    /**
     * Добавление тестовых юзеров
     */
    public function addTestUsers()
    {
        $users = [
            [
                'username' => 'user1',
                'password' => '11111111',
                'email' => 'user1@email.com',
            ],
            [
                'username' => 'user2',
                'password' => '11111111',
                'email' => 'user2@email.com',
            ],
            [
                'username' => 'user3',
                'password' => '11111111',
                'email' => 'user3@email.com',
            ],
        ];

        foreach ($users as $userData) {
            $this->addUser($userData);
        }
    }

    /**
     * Добавление пользователя по данными из массива $userData
     * @param array $userData
     */
    private function addUser(array $userData)
    {
        if (User::findOne(['username' => $userData['username']])) {
            return;
        }

        $user = new User();
        $user->username = $userData['username'];
        $user->email = $userData['email'];
        $user->status = User::STATUS_ACTIVE;
        $user->setPassword($userData['password']);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->save();
    }

}
