<?php


namespace common\services\user;

use Exception;
use Yii;

class UserService
{
    /**
     * @return int|string
     * @throws Exception
     */
    public function getCurrentUserId()
    {
        $user = $userModel = Yii::$app->user->identity;
        if ($user) {
            return $user->getId();
        } else {
            throw new Exception('Пользователь не авторизован');
        }
    }

    /**
     * @param int $userId
     * @return string
     * @throws Exception
     */
    public function getUserActiveBankAccount(int $userId) : string
    {
        //TODO заглушка получения активного банковского счета пользователя
        if (true) {
            return '8723384182348';
        } else {
            throw new Exception('У пользователя нет счета');
        }
    }

    /**
     * @param int $userId
     * @param int $bonus
     * @return bool
     * @throws Exception
     */
    public function setUserBonus(int $userId, int $bonus)
    {
        //TODO заглушка начисления бонуса пользователю
        if (true) {
            return true;
        } else {
            throw new Exception('Бонус начислить не удалось');
        }
    }
}