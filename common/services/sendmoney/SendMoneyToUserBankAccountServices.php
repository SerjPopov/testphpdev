<?php

namespace common\services\sendmoney;

use common\models\prize\Prize;
use common\services\bank\BankApiService;
use common\services\prize\PrizeService;
use common\services\user\UserService;
use Exception;
use Yii;

class SendMoneyToUserBankAccountServices
{
    private $sendLimit = 10;

    public function start()
    {
        Yii::info("Выполнение операции направления денежных призов на счета пользователей sendLimit = $this->sendLimit");
        $prizeService = new PrizeService();

        //Получим неотправленные денежные призы
        $prizes = $prizeService->getNotSendedMoneyPrizes();

        //получим по ним счета для отправки
        $prizesForSend = $this->generateDataForSend($prizes);

        //направим на API банка
        foreach ($prizesForSend as $dataForSend) {
            if ($this->sendToBank($dataForSend)) {
                $prizeService->setStatusUsed($dataForSend['prizeId']);
            }
        }
        Yii::info("Операция завершена");
    }

    /**
     * @param array $prizes
     * @return array
     */
    private function generateDataForSend(array $prizes)
    {
        $prizesForSend = [];
        $count = 0;
        $userService = new UserService();
        /** @var Prize $prize */
        foreach ($prizes as $prize) {
            try {
                $bankAccount = $userService->getUserActiveBankAccount($prize->user_id);
                $prizesForSend[] = [
                    'prizeId' => $prize->id,
                    'userId' => $prize->user_id,
                    'bankAccount' => $bankAccount,
                    'money' => $prize->value,
                ];
                $count++;
                if ($count >= $this->sendLimit) {
                    break;
                }
            } catch (Exception $e) {
                Yii::info("У пользователя {$prize->user_id} нет банковского счета");
            }
        }
        return $prizesForSend;
    }

    /**
     * @param array $dataForSend
     * @return bool
     */
    private function sendToBank(array $dataForSend)
    {
        try {
            (new BankApiService())->sendMoney($dataForSend['bankAccount'], $dataForSend['money']);
            Yii::info("Успешная отправка на счет {$dataForSend['bankAccount']} пользователя {$dataForSend['user_id']}");
            return true;
        } catch (Exception $e) {
            Yii::info("Ошибка ответа API банка при отправке на счет {$dataForSend['bankAccount']} пользователя {$dataForSend['user_id']}");
            return false;
        }
    }
}