<?php


namespace common\services\prize;

use Exception;

class PrizeBonus implements Prize
{
    private $interval = [500, 1000];

    /**
     * @return int
     */
    public function getPrizeValue()
    {
        return rand($this->interval[0], $this->interval[1]);

    }
}