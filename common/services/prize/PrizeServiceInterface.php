<?php

namespace common\services\prize;

use common\models\prize\Prize;
use common\services\user\UserService;

interface PrizeServiceInterface
{
    public function isReadyPrize(int $userId): bool;

    public function getNotSendedMoneyPrizes();

    public function createNewRandomPrize(int $userId): Prize;

    public function convertMoneyToBonus(int $prizeId, PrizeMoney $prizeMoney, UserService $userService);
}