<?php

namespace common\services\prize;

use common\models\prize\Prize;
use common\services\user\UserService;
use Exception;
use Yii;

class PrizeService implements PrizeServiceInterface
{

    private $ratioConvertMoneyToBonus = 100;

    public function isReadyPrize(int $userId): bool
    {
        $prize = Prize::findOne(['user_id' => $userId]);
        if ($prize) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * @return Prize[]
     */
    public function getNotSendedMoneyPrizes()
    {
        return Prize::find()
            ->where([
                'status' => Prize::STATUS_CREATED,
                'type' => Prize::TYPE_MONEY,
            ])
            ->all();
    }

    /**
     * @param int $userId
     * @return Prize
     * @throws Exception
     */
    public function createNewRandomPrize(int $userId): Prize
    {
        $prizes = Prize::getPrizeTypes();
        $prizeType = array_rand($prizes);
        switch ($prizeType) {
            case Prize::TYPE_MONEY :
                try {
                    return $this->createMoneyPrize($userId);
                } catch (Exception $e) {
                    return $this->createBonusPrize($userId);
                }
            case Prize::TYPE_BONUS:
                return $this->createBonusPrize($userId);
            case Prize::TYPE_OBJECT:
                return $this->createObjectPrize($userId);
        }

    }

    /**
     * @param int $prizeId
     * @return bool
     */
    public function setStatusUsed(int $prizeId)
    {
        $prize = Prize::findOne($prizeId);
        if ($prize) {
            $prize->status = Prize::STATUS_USED;
            return $prize->save();
        } else {
            return false;
        }
    }

    /**
     * @param int $prizeId
     * @param PrizeMoney $prizeMoney
     * @param UserService $userService
     * @throws Exception
     */
    public function convertMoneyToBonus(int $prizeId, PrizeMoney $prizeMoney, UserService $userService)
    {
        $prize = Prize::findOne($prizeId);
        if (!$prize) {
            throw new Exception('Приз не найден');
        }
        if ($prize->type !== Prize::TYPE_MONEY) {
            throw new Exception('Приз не денежный, конвертировать нельзя');
        }
        if ($prize->status == Prize::STATUS_USED) {
            throw new Exception('Приз уже использован, конвертировать нельзя');
        }
        $transaction = Yii::$app->db->beginTransaction();
        $money = (int) $prize->value;
        $bonus = $money * $this->ratioConvertMoneyToBonus;
        try {
            $prizeMoney->returnMoney($money);
            $userService->setUserBonus($prize->user_id, $bonus);
        } catch (Exception $e) {
            $transaction->rollBack();
            throw new Exception('Ошибка при возврате денежных средств или зачислении бонуса на счет пользователя');
        }
        $prize->type = Prize::TYPE_BONUS;
        $prize->status = Prize::STATUS_USED;
        $prize->value = (string) $bonus;
        if ($prize->save()) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
            throw new Exception('Конвертирование денежного приза в бонусные быллы не удалось');
        }
    }

    /**
     * @param int $userId
     * @return Prize
     * @throws Exception
     */
    private function createMoneyPrize(int $userId): Prize
    {
        $transaction = Yii::$app->db->beginTransaction();
        $prize = new Prize();

        try {
            $prize->value = (string)(new PrizeMoney())->getPrizeValue();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw new Exception('В копилке нет денег для денежного приза');
        }

        $prize->user_id = $userId;
        $prize->type = Prize::TYPE_MONEY;
        $prize->status = Prize::STATUS_CREATED;

        if ($prize->save()) {
            $transaction->commit();
            return $prize;
        } else {
            $transaction->rollBack();
            throw new Exception('Не удалось создать денежный приз');
        }
    }

    /**
     * @param int $userId
     * @return Prize
     * @throws Exception
     */
    private function createBonusPrize(int $userId): Prize
    {
        $prize = new Prize();
        $prize->value = (string)(new PrizeBonus())->getPrizeValue();
        $prize->user_id = $userId;
        $prize->type = Prize::TYPE_BONUS;
        $prize->status = Prize::STATUS_CREATED;

        if ($prize->save()) {
            return $prize;
        } else {
            throw new Exception('Не удалось создать бонусный приз');
        }
    }

    /**
     * @param int $userId
     * @return Prize
     * @throws Exception
     */
    private function createObjectPrize(int $userId): Prize
    {
        $prize = new Prize();
        $prize->value = 'Предмет';
        $prize->user_id = $userId;
        $prize->type = Prize::TYPE_OBJECT;
        $prize->status = Prize::STATUS_CREATED;

        if ($prize->save()) {
            return $prize;
        } else {
            throw new Exception('Не удалось создать призовой предмет');
        }
    }

    /**
     * @param int $ratioConvertMoneyToBonus
     */
    public function setRatioConvertMoneyToBonus(int $ratioConvertMoneyToBonus)
    {
        $this->ratioConvertMoneyToBonus = $ratioConvertMoneyToBonus;
    }

}