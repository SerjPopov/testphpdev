<?php


namespace common\services\prize;


interface Prize
{
    public function getPrizeValue();
}