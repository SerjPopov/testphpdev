<?php


namespace common\services\prize;

use Exception;

class PrizeMoney implements Prize
{
    private $interval = [10, 20];

    /**
     * @return int
     * @throws Exception
     */
    public function getPrizeValue()
    {
        $money = rand($this->interval[0], $this->interval[1]);
        //TODO обращаемся в мешочек с деньгами и отнимаем там данную сумму и сохраняем в базе, если денег не хватает то генерим исключение
        if (true) {
            return $money;
        } else {
            throw new Exception('В мешочке денег нет');
        }

    }

    /**
     * @param int $money
     * @return bool
     * @throws Exception
     */
    public function returnMoney(int $money)
    {
        //TODO возвращаем в мешочек с деньгами, если не получилось вернуть то бросаем исключение
        if (true) {
            return true;
        } else {
            throw new Exception('Деньги вернуть не удалось');
        }
    }
}