<?php

namespace common\models\prize;

use common\models\User;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "prize".
 *
 * @property int $id
 * @property int $user_id
 * @property int $type
 * @property string $value
 * @property int $status
 *
 * @property User $user
 * @property string $prizeType
 * @property string $prizeStatus
 */
class Prize extends ActiveRecord
{
    const STATUS_CREATED = 0;
    const STATUS_USED = 1;
    const STATUS_REJECTED = 2;

    const TYPE_MONEY = 0;
    const TYPE_BONUS = 1;
    const TYPE_OBJECT = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prize';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'value'], 'required'],
            [['user_id', 'type', 'status'], 'integer'],
            [['value'], 'string'],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'type' => 'Type',
            'value' => 'Value',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Возвращает массив возможных типов призов
     *
     * @return string[]
     */
    public static function getPrizeTypes()
    {
        return [
            self::TYPE_MONEY => 'Денежный',
            self::TYPE_BONUS => 'Бонусные баллы',
            self::TYPE_OBJECT => 'Предмет',
        ];
    }

    /**
     * Возвращает тип приза по его id
     * @param int $typeId
     * @return string
     */
    public function getPrizeType(int $typeId)
    {
        return self::getPrizeTypes()[$typeId] ?? 'Неизвестный тип';
    }

    /**
     * Возвращает массив возможных статусов призов
     *
     * @return string[]
     */
    public static function getPrizeStatuses()
    {
        return [
            self::STATUS_CREATED => 'Создан',
            self::STATUS_USED => 'Использован',
            self::STATUS_REJECTED => 'Отклонен',
        ];
    }

    /**
     * Возвращает статус приза по его id
     * @param int $statusId
     * @return string
     */
    public function getPrizeStatus(int $statusId)
    {
        return self::getPrizeStatuses()[$statusId] ?? 'Неизвестный статус';
    }
}
