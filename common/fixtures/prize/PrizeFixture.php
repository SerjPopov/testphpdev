<?php
namespace common\fixtures\prize;

use yii\test\ActiveFixture;

class PrizeFixture extends ActiveFixture
{
    public $modelClass = 'common\models\prize\Prize';
}