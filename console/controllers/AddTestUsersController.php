<?php

namespace console\controllers;

use common\services\user\TestUserService;
use yii\console\Controller;

class AddTestUsersController extends Controller
{
    /**
     * Добавление тестовых данных в таблицу User
     */
    public function actionAddTestUsers()
    {
        (new TestUserService())->addTestUsers();
    }

}