<?php

namespace console\controllers;

use common\services\sendmoney\SendMoneyToUserBankAccountServices;
use yii\console\Controller;

class SendMoneyToUserBankAccountController extends Controller
{
    public function actionStart()
    {
        (new SendMoneyToUserBankAccountServices())->start();
    }
}