<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%prize}}`.
 */
class m210106_144110_create_prize_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%prize}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unique(),
            'type' => $this->smallInteger()->notNull(),
            'value' => $this->text()->notNull(),
            'status' => $this
                ->smallInteger()
                ->defaultValue(0)
                ->notNull(),
        ]);

        $this->createIndex(
            'type',
            '{{%prize}}',
            'type'
        );

        $this->addForeignKey(
            'prize_to_user',
            '{{%prize}}',
            'user_id',
            '{{%user}}',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%prize}}');
    }
}
