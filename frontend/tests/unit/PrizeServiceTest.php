<?php namespace frontend\tests;

use Codeception\Test\Unit;
use common\fixtures\prize\PrizeFixture;
use common\fixtures\UserFixture;
use common\models\prize\Prize;
use common\services\prize\PrizeMoney;
use common\services\prize\PrizeService;
use common\services\user\UserService;
use Exception;

class PrizeServiceTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
        $this->tester->haveFixtures([
            'prize' => [
                'class' => PrizeFixture::class,
                'dataFile' => codecept_data_dir() . 'prize/prize.php',
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user/user.php',
            ],

        ]);
    }

    /**
     * Отрицательный сценарий
     */
    public function testConvertNotFoundPrizeToBonusPrize()
    {
        $priseId = -1; // несуществующий приз
        $exceptionMessage = '';
        $prizeMoney = $this->makeEmpty(PrizeMoney::class);
        $userService = $this->makeEmpty(UserService::class);
        try {
            (new PrizeService())->convertMoneyToBonus($priseId, $prizeMoney, $userService);
        } catch (Exception $e) {
            $exceptionMessage = $e->getMessage();
        }
        expect($exceptionMessage)->equals('Приз не найден');
    }

    /**
     * Отрицательный сценарий
     */
    public function testConvertNotMoneyPrizeToBonusPrize()
    {
        $priseId = 3; // неденежный приз
        $exceptionMessage = '';
        $prizeMoney = $this->makeEmpty(PrizeMoney::class);
        $userService = $this->makeEmpty(UserService::class);
        try {
            (new PrizeService())->convertMoneyToBonus($priseId, $prizeMoney, $userService);
        } catch (Exception $e) {
            $exceptionMessage = $e->getMessage();
        }
        expect($exceptionMessage)->equals('Приз не денежный, конвертировать нельзя');
    }

    /**
     * Отрицательный сценарий
     */
    public function testConvertUsedPrizeToBonusPrize()
    {
        $priseId = 2; // использованный приз
        $exceptionMessage = '';
        $prizeMoney = $this->makeEmpty(PrizeMoney::class);
        $userService = $this->makeEmpty(UserService::class);
        try {
            (new PrizeService())->convertMoneyToBonus($priseId, $prizeMoney, $userService);
        } catch (Exception $e) {
            $exceptionMessage = $e->getMessage();
        }
        expect($exceptionMessage)->equals('Приз уже использован, конвертировать нельзя');
    }

    /**
     * Положительный сценарий
     * 1) Проверяет, что не было исключений при выполнении теста
     * 2) Проверяет наличие ожидаемых изменений в БД
     */
    public function testConvertMoneyPrizeToBonusPrize()
    {
        $priseId = 1; // денежный вновь созданный приз
        $prizeMoney = $this->makeEmpty(PrizeMoney::class);
        $userService = $this->makeEmpty(UserService::class);
        try {
            (new PrizeService())->convertMoneyToBonus($priseId, $prizeMoney, $userService);
        } catch (Exception $e) {
            expect($e->getMessage())->equals('');
        }
        $this->tester->seeRecord(Prize::class,['id' => $priseId, 'type' => Prize::TYPE_BONUS]);
    }
}