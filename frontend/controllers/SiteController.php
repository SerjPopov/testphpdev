<?php

namespace frontend\controllers;

use common\services\prize\PrizeService;
use common\services\user\UserService;
use Exception;
use Yii;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        try {
            $userId = (new UserService())->getCurrentUserId();
            $isReadyPrize = (new PrizeService())->isReadyPrize($userId);
            $text = $isReadyPrize ? 'Получайте приз' : 'Приз уже получен';
        } catch (Exception $e) {
            $isReadyPrize = false;
            $text = 'Для получения приза авторизуйтесь';
        }

        return $this->render('index',
            [
                'isReadyPrize' => $isReadyPrize,
                'text' => $text,
            ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Создание случайного приза.
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionCreateNewRandomPrize()
    {
        try {
            $userId = (new UserService())->getCurrentUserId();
        } catch (Exception $e) {
            throw new BadRequestHttpException('Пользователь не авторизован');
        }

        try {
            $prize = (new PrizeService())->createNewRandomPrize($userId);
        } catch (Exception $e) {
            throw new BadRequestHttpException('Ошибка при создании приза');
        }

        return $this->render('index',
            [
                'isReadyPrize' => false,
                'text' => 'Ваш приз - ' . VarDumper::dumpAsString($prize),
            ]);
    }
}
