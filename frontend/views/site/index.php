<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $isReadyPrize bool */
/* @var $text string */

$this->title = 'My Yii Application';

?>
<div class="site-index">

    <div class="jumbotron">
        <?php
        if ($isReadyPrize) {
            ?>
                <p><?= Html::a('Получить случайный приз', Url::toRoute('site/create-new-random-prize'), ['class' => 'btn btn-lg btn-success']) ?></p>
            <?php
        } else {
            ?>
                <p><?= $text ?></p>
            <?php
        } ?>
    </div>

    <div class="body-content">

    </div>
</div>
