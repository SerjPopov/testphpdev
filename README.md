1. Склонировать приложение и выполнить: composer install
2. Скомпилить docker контейнеры: docker-compose build
3. Запуск docker контейнеров: docker-compose up -d
4. Команды выполняются в docker контейнере app-test

* Выполнить инициализацию YII2 для dev окружения: php init

* Выполнить миграции: php yii migrate

* Добавить тестовых пользователей: php yii add-test-users/add-test-users

5. Доступ на frontend: localhost:8020

6. Для тестового задания (команды выполняются в docker контейнере app-test):

* Консольная команда для отправки денежных призов пользователям: php yii send-money-to-user-bank-account/start

* Запуск unit теста для проверки конвертации денежного приза в баллы лояльности: 
В каталоге /var/www/frontend выполнить команду ../vendor/bin/codecept run unit PrizeServiceTest